# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: The goal of this programming assignment is to enable you to gain experience programming with:
• Amazon Web Services, specifically the EC2 cloud (http://aws.amazon.com/ec2/), the SQS queuing service (http://aws.amazon.com/sqs/), and the CloudWatch monitoring system (http://aws.amazon.com/cloudwatch/).
• You will learn about dynamic resource provisioning, and how applications can be made autonomic and adaptive
* Version: 1.0


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact