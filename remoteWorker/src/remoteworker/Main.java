/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package remoteworker;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.ClasspathPropertiesFileCredentialsProvider;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;


/**
 *
 * @author Rohan_Kachewar
 */
public class Main {


    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        int idleTime = 0;
        int i = 0;
        

         while (i < args.length) {            
            if (args[i].equals("-i"))
            {                
                i = i+1;
                idleTime = Integer.parseInt(args[i]);
             }
            
            i++;
        }
        System.out.println("the Idle time out (in secs) set for this worker is: " + idleTime);


        AWSCredentials credentials = new PropertiesCredentials(
        InlineGettingStartedCodeSampleApp.class.getResourceAsStream("Credentials.properties"));

        AmazonEC2Client ec2 = new AmazonEC2Client(credentials);

        AmazonSQS sqs = new AmazonSQSClient(credentials);
		Region usEast = Region.getRegion(Regions.US_EAST_1);
		sqs.setRegion(usEast);
                                   
        // Get Input queue
        System.out.println("Getting queue url of SQS Input queue. \n");
        CreateQueueRequest createQueueRequest = new CreateQueueRequest("InputQueue");
        String myQueueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();


        String instanceId = null;
        while(true)
      {

        //Pop messages from Input Queue
        System.out.println("Pop message from Input Queue.\n");
        ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(myQueueUrl);
        List<Message> messages = sqs.receiveMessage(receiveMessageRequest).getMessages();

        //Logic to kill remote worker after idle time
        if ((messages.isEmpty()) && (idleTime != 0)) {
            System.out.println("Time to Sleep for idle timeout");
            Thread.sleep(idleTime*1000);
            messages = sqs.receiveMessage(receiveMessageRequest).getMessages();
            if (messages.isEmpty()) {
                System.out.println("Time to terminate");
                try {
                    String wget = "wget -q -O - http://169.254.169.254/latest/meta-data/instance-id";
                    Process p = Runtime.getRuntime().exec(wget);
                    p.waitFor();
                    BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
                    instanceId = br.readLine();
                  } catch(Exception e) {
                    e.printStackTrace();
                  }
                  if(instanceId.equals("")) return;

                  List<String> instancesToTerminate = new ArrayList<String>();
                  instancesToTerminate.add(instanceId);
                  TerminateInstancesRequest terminateRequest = new TerminateInstancesRequest();
                  terminateRequest.setInstanceIds(instancesToTerminate);
                  ec2.terminateInstances(terminateRequest);
                  break;
            }
        }

        for (Message message : messages) {
            System.out.println("  Message");
            System.out.println("    MessageId:     " + message.getMessageId());
            System.out.println("    ReceiptHandle: " + message.getReceiptHandle());
            System.out.println("    MD5OfBody:     " + message.getMD5OfBody());
            System.out.println("    Body:          " + message.getBody());
            String sleepTime = message.getBody();
            System.out.println("Sleeping for " + sleepTime + " seconds");
            Thread.sleep(Integer.parseInt(sleepTime));
        }


        // Delete the message which was poped to read
        if (!messages.isEmpty())
        {
            System.out.println("Deleting a message.\n");
            String messageRecieptHandle = messages.get(0).getReceiptHandle();
            sqs.deleteMessage(new DeleteMessageRequest(myQueueUrl, messageRecieptHandle));

        
        // Get Result queue
        System.out.println("Getting queue url of SQS Input queue. \n");
        CreateQueueRequest createQueueRequest1 = new CreateQueueRequest("ResultQueue");
        String resultQueueUrl = sqs.createQueue(createQueueRequest1).getQueueUrl();        

        // Update task completed in result Queue
        System.out.println("Sending a message to Result Queue.\n");
        sqs.sendMessage(new SendMessageRequest(resultQueueUrl, "1"));
        sqs.notifyAll();
       }
    }
}

}
