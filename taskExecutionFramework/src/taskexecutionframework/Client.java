/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taskexecutionframework;

import java.io.*;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Rohan_Kachewar
 */
public class Client implements Runnable{

    private int threadID;
    private Socket clientSocket;
    public List<String> anArray = new ArrayList();

    public Client(String IP, int port, String stringFileLocation) throws IOException {
        clientSocket = new Socket(IP, port);
        //clientSocket.setReceiveBufferSize(recieveBufferSize);
        //clientSocket.setSendBufferSize(sendBufferSize);
        System.out.println(clientSocket);

        //Now open the file and get each sleep command
        try
            {
                BufferedReader fstream = new BufferedReader(new FileReader(stringFileLocation)); //true tells to append data.
                String strLine;
                int i = 1;

                while ((strLine = fstream.readLine()) != null)   {
                         // Print the content on the console
                         System.out.println (strLine);
                         //Fill the sleep numbers in an array to send to client
                         String sleepCount = strLine.replaceAll("[a-zA-Z\\W]", "");
                         System.out.println (sleepCount);
                         anArray.add(sleepCount);
                         i++;
                }
                fstream.close();
            }
        catch (Exception e)
            {
                System.err.println("Error: " + e.getMessage());
            }

   }


   public void startClient() throws Exception
    //public static void main(String argv[]) throws Exception
   {
        System.out.println("This is a client side test sentence for thread id: ");
        String sentence = "This is a test sentence for thread id: ";
        String modifiedSentence;
        System.out.println(this.clientSocket);

        DataOutputStream outToServer =
          new DataOutputStream(this.clientSocket.getOutputStream());

        BufferedReader inFromServer =
          new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

        //sentence = inFromUser.readLine();

        int taskCounter = 0;
        //Send the parsed sleep number array to Server one by one
        for(String z : anArray){
            taskCounter++;
            System.out.println("Next");
            System.out.println(z);
            outToServer.writeBytes(z + '\n');
            modifiedSentence = inFromServer.readLine();
            System.out.println("Client Recieved Acknowledge from server: " + modifiedSentence);
            Thread.sleep(10);
            //outToServer.close();
        }
            System.out.println("Total tasks send: " + taskCounter);
            String sentence1 = "Completed" + '\n';
            outToServer.writeBytes(sentence1);
            Thread.sleep(10);
            String taskResultCounter = null;
            System.out.println(Integer.toString(taskCounter));

            //We keep on pooling untill send tasks are == recieved tasks
            while(!(Integer.toString(taskCounter).equals(taskResultCounter)))
            {                
                taskResultCounter = inFromServer.readLine();
                System.out.println("Total tasks Recieved: " + taskResultCounter);
                //Validation to check if all tasks have been completed or not
                if (Integer.toString(taskCounter).equals(taskResultCounter))
                {
                    System.out.println("All Tasks Completed Successfully");
                }else
                {
                    System.out.println("Tasks completed but some task might be delaying");
                }
            }

            this.clientSocket.close();

    }

   public void run() {
        try {
            this.startClient();
        } catch (Exception ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
