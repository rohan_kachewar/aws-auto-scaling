/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskexecutionframework;

import java.io.IOException;

/**
 *
 * @author Rohan_Kachewar
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        System.out.println("******************************* Starting Client *******************************");        
        //format of file path: C:\\12.txt
        int i=0;
        String IP = null,filePath = null;
        int port = 0;
        while (i < args.length) {
            if (args[i].equals("-s"))
            {
                i = i+1;
                String address = args[i];
                String[] array = address.split(":");
                IP = array[0];
                port = Integer.parseInt(array[1]);
             }

            if (args[i].equals("-w"))
            {
                i = i+1;
                filePath = args[i];
             }

            i++;
        }

        System.out.println(" " + IP + " " + port + " " + filePath);
        Runnable r = new Client(IP, port, filePath);
        Thread t;
        t = new Thread(r);
        System.out.println("Starting Thread:");
        t.start();

    }

}
