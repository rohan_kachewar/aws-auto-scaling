/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskexecutionframeworkserver;

/**
 *
 * @author Rohan_Kachewar
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        System.out.println("*******************************Starting Server*******************************");

        int i=0;
        Boolean remoteW = false;
        int port = 0;
        int numOfLocalW = 0;
        while (i < args.length) {
            if (args[i].equals("-s"))
            {
                i = i+1;
                port = Integer.parseInt(args[i]);
             }

            if (args[i].equals("-rw"))
            {
                remoteW = true;
            }

            if (args[i].equals("-lw"))
            {
                 i = i+1;
                 numOfLocalW = Integer.parseInt(args[i]);
            }

            i++;
        }

        System.out.println(" " + port + " " + numOfLocalW + " " + remoteW);

        Server server = new Server(port,numOfLocalW, remoteW);
        server.startServer();
    }

}
