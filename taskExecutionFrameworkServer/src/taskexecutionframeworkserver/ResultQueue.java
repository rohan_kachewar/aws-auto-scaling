/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskexecutionframeworkserver;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 *
 * @author Rohan_Kachewar
 */
public class ResultQueue {
    //This Queue class is a thread safe (written in house) class
    public static Queue<Object> readQ = new ArrayDeque <Object>(100);

    public static void enqueue(Object object)
    {
        //do some stuff
        readQ.add(object);
    }

    public static Object dequeue()
    {
        //do some stuff
        return readQ.remove();
    }

    public static boolean isEmpty()
    {
        //do some stuff
        return readQ.isEmpty();
    }

}
