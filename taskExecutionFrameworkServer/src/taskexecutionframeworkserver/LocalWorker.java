/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskexecutionframeworkserver;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rohan_Kachewar
 */
public class LocalWorker extends Thread {

    int duration;
    int taskID;
    public LocalWorker(int i) {
        Object obj = QueueHandler.dequeue();
        String objStr = obj.toString();
        duration = Integer.parseInt(objStr);
        taskID = i;
    }


    public void run() {
        try {
            System.out.println("Executing Job ID: " + Integer.toString(taskID) + " and sleep of: " + Integer.toString(duration));
            Thread.sleep(duration);
            ResultQueue.enqueue(taskID);


        } catch (InterruptedException ex) {
            Logger.getLogger(LocalWorker.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}
