/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskexecutionframeworkserver;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rohan_Kachewar
 */
public class Server{

    private int port;
    ServerSocket welcomeSocket;
    private Socket connectionSocket;
    private String clientSentence;
    private String capitalizedSentence;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;
    private ExecutorService pool;
    private CompletionService<String> threadPool;
    public List<String> anArray = new ArrayList();
    private Boolean rw;
    private AmazonSQS inputSQS, resultSQS;
    private int flag = 0;



    public Server(int serverPort, int numOfThreads, Boolean rwOption) {
        port = serverPort;
        numOfThreads = numOfThreads;
        System.out.println(numOfThreads);
        pool = Executors.newFixedThreadPool(numOfThreads);
        rw = rwOption;        
   }




      public AmazonSQS initializeQueue(String queueName) throws IOException {
        AWSCredentials credentials = new PropertiesCredentials(
                InlineGettingStartedCodeSampleApp.class.getResourceAsStream("Credentials.properties"));
        AmazonSQS sqs = new AmazonSQSClient(credentials);
		Region usEast = Region.getRegion(Regions.US_EAST_1);
		sqs.setRegion(usEast);

        System.out.println("===========================================");
        System.out.println("Getting Started with Amazon SQS");
        System.out.println("===========================================\n");

        try {
            // Create a queue
            System.out.println("Creating a new SQS queue called: " + queueName + "\n");
            CreateQueueRequest createQueueRequest = new CreateQueueRequest(queueName);
            String myQueueUrl = sqs.createQueue(createQueueRequest).getQueueUrl();
            // List queues
            System.out.println("Listing all queues in your account.\n");
            for (String queueUrl : sqs.listQueues().getQueueUrls()) {
                System.out.println("  QueueUrl: " + queueUrl);
            }
            System.out.println();

        } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
        return sqs;
    }

      public void startServer() throws Exception
      {
         String clientSentence;
         ServerSocket welcomeSocket = new ServerSocket(port);
         System.out.println(welcomeSocket);
         Socket connectionSocket = welcomeSocket.accept();
         while(true)
         {
             try {
                clientSentence = null;
                System.out.println("Server Listening1");
                inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                outToClient = new DataOutputStream(connectionSocket.getOutputStream());
                System.out.println("Server Listening: ");
                clientSentence = inFromClient.readLine();
                System.out.println(clientSentence);
//----------------------------------------------------------------------------------------------------------------
//Part for local workers
               if (!rw)
               {
                if (clientSentence != null){
                   if (!clientSentence.equals("Completed")&&!clientSentence.equals("Check")) {
                    System.out.println("Server Got command");
                    System.out.println(clientSentence);
                    //anArray.add(clientSentence);
                    QueueHandler.enqueue(clientSentence);
                    QueueHandler.printQueue();
                    String clientSentence2 = "Recieved" + '\n';
                    outToClient.writeBytes(clientSentence2);
                    }

                    if (clientSentence.equals("Completed")){
                        System.out.println("Server: In Completed method");
                        Thread[] workers = new Thread[4];
                        int i = 0;
                        System.out.println(QueueHandler.queueLength());
                        int len = QueueHandler.queueLength();
                        while (i<len){
                            System.out.println(i);
                            workers[i] = new LocalWorker(i);
                            System.out.println(workers[i]);
                            pool.submit(workers[i]);
                            i++;
                            }
                        //Check if required
                        System.out.println("Step 1");
                        pool.shutdown();
                        System.out.println("Step 2");


                    System.out.println("Step 3");
                    int taskCounter = 0;
                    //if (clientSentence.contains("Check")){
                        Thread.sleep(100);
                        System.out.println("Server: In Check method ");

                        while(ResultQueue.isEmpty())
                        {
                            System.out.println("Waiting for results to arrive");
                        }

                        while(!ResultQueue.isEmpty())
                            {
                            Object obj = ResultQueue.dequeue();
                            taskCounter++;

                            if(ResultQueue.isEmpty())
                            {
                                //Sleep for 10 seconds to be sure that result queue empty signifies that all tasks have completed
                                Thread.sleep(10000);
                            }
                        }

                        String result = Integer.toString(taskCounter) + "\n";
                        System.out.println("Sending result: " + result);
                        outToClient.writeBytes(result);
                    }
                }
            }
//----------------------------------------------------------------------------------------------------------------
//Part for remote workers
               else {

                if (clientSentence != null){
                   if (!clientSentence.equals("Completed")&&!clientSentence.equals("Check")) {
                    System.out.println("Server Got command");
                    System.out.println(clientSentence);
                    //Initialize for queue creation
                    AWSCredentials credentials = new PropertiesCredentials(
                    InlineGettingStartedCodeSampleApp.class.getResourceAsStream("Credentials.properties"));
                    inputSQS = new AmazonSQSClient(credentials);
                    resultSQS = new AmazonSQSClient(credentials);
                    Region usEast = Region.getRegion(Regions.US_EAST_1);
                    inputSQS.setRegion(usEast);
                    resultSQS.setRegion(usEast);

                    //Create/Reuse Input SQS
                    CreateQueueRequest createInputQueueRequest = new CreateQueueRequest("InputQueue");
                    String inputQueueURL = inputSQS.createQueue(createInputQueueRequest).getQueueUrl();

                    //Send the message to Input Queue
                    System.out.println("Inserting in Input Queue: " + clientSentence);
                    inputSQS.sendMessage(new SendMessageRequest(inputQueueURL, clientSentence));
                    System.out.println(inputQueueURL);

                    //Create Result SQS
                    CreateQueueRequest createResultQueueRequest = new CreateQueueRequest("ResultQueue");
                    resultSQS.createQueue(createResultQueueRequest).getQueueUrl();


                    //Signal Client the task is added to Queue
                    String clientSentence2 = "Recieved" + '\n';
                    outToClient.writeBytes(clientSentence2);
                    }

                    if (clientSentence.equals("Completed")){
                        System.out.println("Server recieved all tasks and now creating an alarm on CloudWatch");
                        //Now configure the Alarm on CloudWatch
                        CloudWatchPolicySetter cp = new CloudWatchPolicySetter();
                        cp.setAlarm();
                    

                    //if (clientSentence.contains("Check")){
                           System.out.println("Server: In Check method");
                           System.out.println("Since there is a lot of latency in starting remote workers and getting result in Result Queue. \n");


                           CreateQueueRequest createQueueRequest = new CreateQueueRequest("ResultQueue");
                           String resultQueueUrl = resultSQS.createQueue(createQueueRequest).getQueueUrl();

                            while(flag == 0)
                            {
                                System.out.println("Starting to wait for messages to arrive in result queue");
                                GetQueueAttributesRequest attribute = new GetQueueAttributesRequest(resultQueueUrl);
                                attribute.setAttributeNames( Arrays.asList("ApproximateNumberOfMessages"));
                                Map map = resultSQS.getQueueAttributes(attribute).getAttributes();
                                String map1 = map.get("ApproximateNumberOfMessages").toString();
                                System.out.println(map1);

                                while(map1.equals("0"))
                                {
                                    System.out.println("Waiting for messages to arrive in result queue");
                                    attribute = new GetQueueAttributesRequest(resultQueueUrl);
                                    attribute.setAttributeNames( Arrays.asList("ApproximateNumberOfMessages"));
                                    map = resultSQS.getQueueAttributes(attribute).getAttributes();
                                    map1 = map.get("ApproximateNumberOfMessages").toString();
                                    System.out.println(map1);
                                }
                                flag = 1;
                            }


                           // Receive messages
                           System.out.println("Pop message from Result Queue.\n");
                           ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(resultQueueUrl);
                           receiveMessageRequest.setMaxNumberOfMessages(10);
                           receiveMessageRequest.setVisibilityTimeout(0);
                           List<Message> messages = resultSQS.receiveMessage(receiveMessageRequest).getMessages();
                           int taskCounter = 0;
                           System.out.println(messages.size());
                           for (Message message : messages)
                           {
                               System.out.println("Task with sleep: " + message.getBody() + " completed");
                               System.out.println(messages.size());
                               taskCounter++;
                               //Now Deleting a message from Result Queue
                               String messageRecieptHandle = messages.get(0).getReceiptHandle();
                               resultSQS.deleteMessage(new DeleteMessageRequest(resultQueueUrl, messageRecieptHandle));
                               //Clear the list
                               //chk by removing below
                               //messages.clear();
                               // Again pop from Result Queue
                               messages = resultSQS.receiveMessage(receiveMessageRequest).getMessages();
                               System.out.println("Hi");
                               System.out.println(messages.size());
                               if (messages.isEmpty())
                               {
                                //If the Result queue is empty wait for idle time out 10 secs to check if any more jobs are yet to be completed
                                System.out.println("Waiting for 10 secs to check if jobs come");
                                Thread.sleep(10000);
                                //Now re check if any new jobs have been added
                                messages = resultSQS.receiveMessage(receiveMessageRequest).getMessages();
                               }
                           }

                           //Send taskCounter back to client
                           String clientSentenceRemote = Integer.toString(taskCounter) + "\n";
                           outToClient.writeBytes(clientSentenceRemote);

                     }

                  }


               }//else ends here

           } catch (IOException e)
               {
                System.out.println("Error" + ": " + e);
                }
         }
      }//While ends here

}

