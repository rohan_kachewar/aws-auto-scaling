/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskexecutionframeworkserver;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClient;
import com.amazonaws.services.autoscaling.model.CreateAutoScalingGroupRequest;
import com.amazonaws.services.autoscaling.model.CreateLaunchConfigurationRequest;
import com.amazonaws.services.autoscaling.model.InstanceMonitoring;
import com.amazonaws.services.autoscaling.model.PutScalingPolicyRequest;
import com.amazonaws.services.autoscaling.model.PutScalingPolicyResult;
import com.amazonaws.services.autoscaling.AmazonAutoScaling;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.ComparisonOperator;
import com.amazonaws.services.cloudwatch.model.Dimension;
import com.amazonaws.services.cloudwatch.model.PutMetricAlarmRequest;
import com.amazonaws.services.cloudwatch.model.StandardUnit;
import com.amazonaws.services.cloudwatch.model.Statistic;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rohan_Kachewar
 */
public class CloudWatchPolicySetter {

     //public static void main(String... args) throws IOException {

        public void setAlarm() throws IOException {
    try {
        //First we need a Launch Configuration
        CreateLaunchConfigurationRequest lcRequest = new CreateLaunchConfigurationRequest();
        lcRequest.setLaunchConfigurationName("Test instance19");        
        lcRequest.setImageId("ami-95dcb1fc");
        lcRequest.setInstanceType("m1.small");
        lcRequest.setSpotPrice("0.08");
        lcRequest.setKeyName("hw5_test");

//        /**
//         * EC2 security groups use the friendly name
//         * VPC security groups use the identifier
//         */
        List<String> securityGroups = new ArrayList<String>();
        securityGroups.add("sg-6598660e");
        lcRequest.setSecurityGroups(securityGroups);

        InstanceMonitoring monitoring = new InstanceMonitoring();
        monitoring.setEnabled(Boolean.FALSE);
        lcRequest.setInstanceMonitoring(monitoring);

        AWSCredentials credentials = new PropertiesCredentials(
                InlineGettingStartedCodeSampleApp.class.getResourceAsStream("Credentials.properties"));
        AmazonAutoScalingClient asClient = new AmazonAutoScalingClient(credentials);

        asClient.createLaunchConfiguration(lcRequest);
        
        

        //Now we’ll create the AutoScaling Group
        CreateAutoScalingGroupRequest asgRequest = new CreateAutoScalingGroupRequest();
        asgRequest.setAutoScalingGroupName("TestGroup15");
        asgRequest.setLaunchConfigurationName("Test instance19"); // as above

        List<String> avZones = new ArrayList<String>();
        avZones.add("us-east-1c"); // or whatever you need
        asgRequest.setAvailabilityZones(avZones);

        asgRequest.setMinSize(0);  // disabling it for the moment
        asgRequest.setMaxSize(32); //  disabling it for the moment set it 32 in final code
        
        asClient.createAutoScalingGroup(asgRequest);

        //Wait for few seconds
           try {
                Thread.sleep(5000);
           } catch (InterruptedException e) {               
               e.printStackTrace();
           }


        //To AutoScale we’ll need to define a scaling policy and an alarm, so first the policy
        PutScalingPolicyRequest request = new PutScalingPolicyRequest();
        request.setAutoScalingGroupName("TestGroup15");
        request.setPolicyName("TestPolicy-new-up6"); // This scales up so I’ve put up at the end.
        request.setScalingAdjustment(1); // scale up by one
        request.setAdjustmentType("ChangeInCapacity");

        PutScalingPolicyResult result = asClient.putScalingPolicy(request);
        String arn = result.getPolicyARN(); // You need the policy ARN in the next step so make a note of it.
        System.out.println(arn);

        //Now for the alarm
        //String upArn = "arn:aws:autoscaling:xxx"; // from the policy request
        String upArn = arn;
        // Scale Up
        PutMetricAlarmRequest upRequest = new PutMetricAlarmRequest();
        upRequest.setAlarmName("AlarmName-up6");
        upRequest.setMetricName("ApproximateNumberOfMessagesVisible");

        List<Dimension> dimensions = new ArrayList<Dimension>();
        Dimension dimension = new Dimension();
        dimension.setName("QueueName");
        dimension.setValue("InputQueue");
        dimensions.add(dimension);        
        upRequest.setDimensions(dimensions);

        upRequest.setNamespace("AWS/SQS");
        upRequest.setComparisonOperator(ComparisonOperator.GreaterThanThreshold);
        upRequest.setStatistic(Statistic.Sum);
        upRequest.setUnit(StandardUnit.Count);
        upRequest.setThreshold(2.0);
        upRequest.setPeriod(60);
        upRequest.setEvaluationPeriods(1);

        List<String> actions = new ArrayList<String>();
        actions.add(upArn); // This is the value returned by the ScalingPolicy request
        upRequest.setAlarmActions(actions);

        AmazonCloudWatchClient cw = new AmazonCloudWatchClient(credentials);
        cw.putMetricAlarm(upRequest);
         } catch (AmazonServiceException ase) {
            System.out.println("Caught an AmazonServiceException, which means your request made it " +
                    "to Amazon SQS, but was rejected with an error response for some reason.");
            System.out.println("Error Message:    " + ase.getMessage());
            System.out.println("HTTP Status Code: " + ase.getStatusCode());
            System.out.println("AWS Error Code:   " + ase.getErrorCode());
            System.out.println("Error Type:       " + ase.getErrorType());
            System.out.println("Request ID:       " + ase.getRequestId());
        } catch (AmazonClientException ace) {
            System.out.println("Caught an AmazonClientException, which means the client encountered " +
                    "a serious internal problem while trying to communicate with SQS, such as not " +
                    "being able to access the network.");
            System.out.println("Error Message: " + ace.getMessage());
        }
      }
   }
