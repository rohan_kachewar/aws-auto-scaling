/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taskexecutionframeworkserver;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.autoscaling.AmazonAutoScalingClient;
import com.amazonaws.services.autoscaling.model.DeleteAutoScalingGroupRequest;
import com.amazonaws.services.autoscaling.model.DeleteLaunchConfigurationRequest;
import com.amazonaws.services.autoscaling.model.DeletePolicyRequest;
import com.amazonaws.services.autoscaling.model.UpdateAutoScalingGroupRequest;
import com.amazonaws.services.cloudwatch.AmazonCloudWatchClient;
import com.amazonaws.services.cloudwatch.model.DeleteAlarmsRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rohan_Kachewar
 */
public class Cleanup  {


    public static void main(String[] args) throws IOException {
        AWSCredentials credentials = new PropertiesCredentials(
                InlineGettingStartedCodeSampleApp.class.getResourceAsStream("Credentials.properties"));
        AmazonAutoScalingClient asc = new AmazonAutoScalingClient(credentials);

        AmazonCloudWatchClient cw = new AmazonCloudWatchClient(credentials);



        try {
            // Delete alarms
            DeleteAlarmsRequest delAlarmRequest = new DeleteAlarmsRequest();
            List<String> alarmNames = new ArrayList<String>();
            alarmNames.add("AlarmName-up6");
            delAlarmRequest.setAlarmNames(alarmNames);
            cw.deleteAlarms(delAlarmRequest);
        } catch(Exception e) {
            System.out.println("Exception1:" + e.getMessage());
        }

        try {
            // Delete Policy
            DeletePolicyRequest delPolicyRequest = new DeletePolicyRequest();
            delPolicyRequest.setAutoScalingGroupName("TestGroup15");
            delPolicyRequest.setPolicyName("TestPolicy-new-up6");
            asc.deletePolicy(delPolicyRequest);
        } catch(Exception e) {
                     System.out.println("Exception1:" + e.getMessage());
        }

        try {
            UpdateAutoScalingGroupRequest ureq = new UpdateAutoScalingGroupRequest();
            ureq.setAutoScalingGroupName("TestGroup15");
            ureq.setMaxSize(0);
            ureq.setMinSize(0);
            asc.updateAutoScalingGroup(ureq);
        } catch(Exception e) {
                     System.out.println("Exception1:" + e.getMessage());
        }

        try {
            Thread.sleep(10000);
        } catch(Exception e) {
        }

        try {
            // Delete AutoScaling group
            DeleteAutoScalingGroupRequest delASGRequest = new DeleteAutoScalingGroupRequest();
            delASGRequest.setAutoScalingGroupName("TestGroup15");
            asc.deleteAutoScalingGroup(delASGRequest);
        } catch(Exception e) {
                     System.out.println("Exception1:" + e.getMessage());
        }

   try {
            Thread.sleep(10000);
        } catch(Exception e) {
        }



        try {
            // Delete configuration
            DeleteLaunchConfigurationRequest  delLCRequest = new DeleteLaunchConfigurationRequest();
            delLCRequest.setLaunchConfigurationName("Test instance19");
            asc.deleteLaunchConfiguration(delLCRequest);
        } catch(Exception e) {
            System.out.println("Exception1:" + e.getMessage());
        }
    }


}
